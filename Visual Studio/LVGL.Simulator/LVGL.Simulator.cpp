﻿/*
 * PROJECT:   LVGL ported to Windows Desktop
 * FILE:      LVGL.Windows.Desktop.cpp
 * PURPOSE:   Implementation for LVGL ported to Windows Desktop
 *
 * LICENSE:   The MIT License
 *
 * DEVELOPER: Mouri_Naruto (Mouri_Naruto AT Outlook.com)
 */

#include <Windows.h>
#include "stdio.h"
#include "string.h"
#include "resource.h"
#include "time.h"

#if _MSC_VER >= 1200
 // Disable compilation warnings.
#pragma warning(push)
// nonstandard extension used : bit field types other than int
#pragma warning(disable:4214)
// 'conversion' conversion from 'type1' to 'type2', possible loss of data
#pragma warning(disable:4244)
#endif

#include "lvgl/lvgl.h"
#include "lv_examples/lv_examples.h"
#include "lv_drivers/win32drv/win32drv.h"

#if _MSC_VER >= 1200
// Restore compilation warnings.
#pragma warning(pop)
#endif



//定义图标字体1
#define weather_1 "\xEE\x98\x90"  //晴天
#define weather_2 "\xEE\x9A\x8C"  //阴天
#define weather_3 "\xEE\x98\x8C"  //多云
#define weather_4 "\xEE\x98\x95"  //雨
#define weather_5 "\xEE\x98\x93"  //大雨
#define weather_6 "\xEE\x98\x8D"  //暴雨
#define weather_7 "\xEE\x98\x85"  //阵雨
#define weather_8 "\xEE\x98\x97"  //雪
#define weather_9 "\xEE\x98\x8F"  //大雪
#define weather_10 "\xEE\x98\x9F"  //雾
#define weather_11 "\xEE\x9B\xB2" //无

static const char* Waitwifi = "Wait Wifi Connect";
char wifi_ssid[32] = "nova 5";
char loaction[10] = "dalian";
char wind_direction[10] = "东北";
int wind_scale = 2;
int humi = 79;
lv_obj_t* scr_main;//创建一个屏幕
uint8_t test_data = 0;
//uint8_t state = 0;

typedef struct _lv_clock
{
    lv_obj_t* time_label; // 时间标签
    lv_obj_t* data_label; // 日期标签
    lv_obj_t* weekday_label; //星期标签
    lv_obj_t* weather_label;//天气标签
    lv_obj_t* weather_text_label;//天气标签
    lv_obj_t* temperature_label;//温度标签
}lv_clock_t;

void lv_ex_init(void);
void lv_main_time(void);
static void task_cb(lv_task_t* task)
{
    uint8_t* dat = (uint8_t*)task->user_data;

        printf("tick: %d test_data %d\n", lv_tick_get(), *dat);
        (*dat)++;
        if (*dat == 2) {
            printf("Wifi finish Connect");
            lv_scr_load_anim(scr_main, LV_SCR_LOAD_ANIM_FADE_ON, 1000, 100, true);
            lv_task_del(task);
        }
}


static void clock_date_task_callback(lv_task_t* task)
{
    static const char* week_day[7] = { "Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday" };
    static time_t unix_time;
    static struct tm* time_info;

    unix_time = time(NULL);
    time_info = localtime(&unix_time);

    int year = time_info->tm_year + 1900;
    int month = time_info->tm_mon + 1;
    int day = time_info->tm_mday;
    int weekday = time_info->tm_wday;
    int hour = time_info->tm_hour;
    int minutes = time_info->tm_min;
    int second = time_info->tm_sec;
     
    lv_clock_t* clock = (lv_clock_t*)(task->user_data);

    lv_label_set_text_fmt(clock->time_label, "%02d:%02d:%02d", hour,minutes,second);
    lv_obj_align(clock->time_label, lv_obj_get_parent(clock->time_label), LV_ALIGN_CENTER, 0, -10);

    lv_label_set_text_fmt(clock->data_label, "%d-%02d-%02d", year, month, day);
    lv_obj_align(clock->data_label, lv_obj_get_parent(clock->data_label), LV_ALIGN_IN_BOTTOM_LEFT, 5, -5);

    lv_label_set_text_fmt(clock->weekday_label, "%s", week_day[weekday]);
    lv_obj_align(clock->weekday_label, lv_obj_get_parent(clock->weekday_label), LV_ALIGN_IN_BOTTOM_RIGHT, -5, -5);

    int code = 11;
    int temperature = 30;
    if ((code>=0)&&(code<=3))
    {
        lv_label_set_text(clock->weather_label, weather_1);// 设置显示文本
        lv_obj_align(clock->weather_label, lv_obj_get_parent(clock->weather_label), LV_ALIGN_CENTER, 0, -10);
        lv_label_set_text(clock->weather_text_label, "晴天");// 设置显示文本
        lv_obj_align(clock->weather_text_label, lv_obj_get_parent(clock->weather_text_label), LV_ALIGN_IN_BOTTOM_MID, 0, -5);
    }
    else if ((code >= 4) && (code <= 8))
    {
        lv_label_set_text(clock->weather_label, weather_3);// 设置显示文本
        lv_obj_align(clock->weather_label, lv_obj_get_parent(clock->weather_label), LV_ALIGN_CENTER, 0, -10);
        lv_label_set_text(clock->weather_text_label, "多云");// 设置显示文本
        lv_obj_align(clock->weather_text_label, lv_obj_get_parent(clock->weather_text_label), LV_ALIGN_IN_BOTTOM_MID, 0, -5);
    }
    else if (code == 9)
    {
        lv_label_set_text(clock->weather_label, weather_2);// 设置显示文本
        lv_obj_align(clock->weather_label, lv_obj_get_parent(clock->weather_label), LV_ALIGN_CENTER, 0, -10);
        lv_label_set_text(clock->weather_text_label, "阴天");// 设置显示文本
        lv_obj_align(clock->weather_text_label, lv_obj_get_parent(clock->weather_text_label), LV_ALIGN_IN_BOTTOM_MID, 0, -5);
    }
    else if ((code >= 10) && (code <= 12))
    {
        lv_label_set_text(clock->weather_label, weather_7);// 设置显示文本
        lv_obj_align(clock->weather_label, lv_obj_get_parent(clock->weather_label), LV_ALIGN_CENTER, 0, -10);
        lv_label_set_text(clock->weather_text_label, "阵雨");// 设置显示文本
        lv_obj_align(clock->weather_text_label, lv_obj_get_parent(clock->weather_text_label), LV_ALIGN_IN_BOTTOM_MID, 0, -5);
    }
    else if (code == 13)
    {
        lv_label_set_text(clock->weather_label, weather_4);// 设置显示文本
        lv_obj_align(clock->weather_label, lv_obj_get_parent(clock->weather_label), LV_ALIGN_CENTER, 0, -10);
        lv_label_set_text(clock->weather_text_label, "雨天");// 设置显示文本
        lv_obj_align(clock->weather_text_label, lv_obj_get_parent(clock->weather_text_label), LV_ALIGN_IN_BOTTOM_MID, 0, -5);
    }
    else if ((code >= 14) && (code <= 15))
    {
        lv_label_set_text(clock->weather_label, weather_5);// 设置显示文本
        lv_obj_align(clock->weather_label, lv_obj_get_parent(clock->weather_label), LV_ALIGN_CENTER, 0, -10);
        lv_label_set_text(clock->weather_text_label, "大雨");// 设置显示文本
        lv_obj_align(clock->weather_text_label, lv_obj_get_parent(clock->weather_text_label), LV_ALIGN_IN_BOTTOM_MID, 0, -5);
    }
    else if ((code >= 16) && (code <= 18))
    {
        lv_label_set_text(clock->weather_label, weather_6);// 设置显示文本
        lv_obj_align(clock->weather_label, lv_obj_get_parent(clock->weather_label), LV_ALIGN_CENTER, 0, -10);
        lv_label_set_text(clock->weather_text_label, "暴雨");// 设置显示文本
        lv_obj_align(clock->weather_text_label, lv_obj_get_parent(clock->weather_text_label), LV_ALIGN_IN_BOTTOM_MID, 0, -5);
    }
    else if ((code >= 19) && (code <= 22))
    {
        lv_label_set_text(clock->weather_label, weather_8);// 设置显示文本
        lv_obj_align(clock->weather_label, lv_obj_get_parent(clock->weather_label), LV_ALIGN_CENTER, 0, -10);
        lv_label_set_text(clock->weather_text_label, "雪天");// 设置显示文本
        lv_obj_align(clock->weather_text_label, lv_obj_get_parent(clock->weather_text_label), LV_ALIGN_IN_BOTTOM_MID, 0, -5);
    }
    else if ((code >= 23) && (code <= 25))
    {
        lv_label_set_text(clock->weather_label, weather_9);// 设置显示文本
        lv_obj_align(clock->weather_label, lv_obj_get_parent(clock->weather_label), LV_ALIGN_CENTER, 0, -10);
        lv_label_set_text(clock->weather_text_label, "暴雪");// 设置显示文本
        lv_obj_align(clock->weather_text_label, lv_obj_get_parent(clock->weather_text_label), LV_ALIGN_IN_BOTTOM_MID, 0, -5);
    }
    else if ((code >= 30) && (code <= 31))
    {
        lv_label_set_text(clock->weather_label, weather_10);// 设置显示文本
        lv_obj_align(clock->weather_label, lv_obj_get_parent(clock->weather_label), LV_ALIGN_CENTER, 0, -10);
        lv_label_set_text(clock->weather_text_label, "雾天");// 设置显示文本
        lv_obj_align(clock->weather_text_label, lv_obj_get_parent(clock->weather_text_label), LV_ALIGN_IN_BOTTOM_MID, 0, -5);
    }
    else
    {
        lv_label_set_text(clock->weather_label, weather_11);// 设置显示文本
        lv_obj_align(clock->weather_label, lv_obj_get_parent(clock->weather_label), LV_ALIGN_CENTER, 0, -10);
        lv_label_set_text(clock->weather_text_label, "未知");// 设置显示文本
        lv_obj_align(clock->weather_text_label, lv_obj_get_parent(clock->weather_text_label), LV_ALIGN_IN_BOTTOM_MID, 0, -5);
    }
    lv_label_set_text_fmt(clock->temperature_label, "%d°", temperature);
    lv_obj_align(clock->temperature_label, lv_obj_get_parent(clock->temperature_label), LV_ALIGN_CENTER, 0, 0);
}
void lv_ex_task(void)
{
    lv_task_t* task = lv_task_create(task_cb, 2000, LV_TASK_PRIO_MID, &test_data);
    lv_task_ready(task);
}
int main()
{
    lv_init();
    if (!lv_win32_init(
        GetModuleHandleW(NULL),
        SW_SHOW,
        LV_HOR_RES_MAX,
        LV_VER_RES_MAX,
        LoadIconW(GetModuleHandleW(NULL), MAKEINTRESOURCE(IDI_LVGL))))
    {
        return -1;
    }
    static lv_style_t style_bg;
    lv_style_init(&style_bg);
    lv_style_set_bg_color(&style_bg, LV_STATE_DEFAULT, lv_color_hex(0x41485a));
    scr_main = lv_obj_create(NULL, NULL);//创建主屏幕
    lv_obj_add_style(scr_main, LV_OBJ_PART_MAIN, &style_bg);

    lv_ex_task();
    /*
     * Demos, benchmarks, and tests.
     *
     * Uncomment any one (and only one) of the functions below to run that
     * item.
     */

    // lv_demo_widgets();
    //lv_demo_benchmark();
    //lv_demo_keypad_encoder();
    //lv_demo_printer();
    //lv_demo_stress();

        lv_ex_init();
        lv_main_time();
 //       lv_main_time();

   // lv_ex_get_started_1();
    //lv_ex_get_started_2();
    //lv_ex_get_started_3();

    //lv_ex_style_1();
    //lv_ex_style_2();
    //lv_ex_style_3();
    //lv_ex_style_4();
    //lv_ex_style_5();
    //lv_ex_style_6();
    //lv_ex_style_7();
    //lv_ex_style_8();
    //lv_ex_style_9();
    //lv_ex_style_10();
    //lv_ex_style_11();

    /*
     * There are many examples of individual widgets found under the
     * lv_examples/src/lv_ex_widgets directory.  Here are a few sample test
     * functions.  Look in that directory to find all the rest.
     */
     //lv_ex_arc_1();
     //lv_ex_cpicker_1();
     //lv_ex_gauge_1();
     //lv_ex_img_1();
     //lv_ex_tileview_1();

    while (!lv_win32_quit_signal)
    {
        lv_task_handler();
        Sleep(10);
    }

    return 0;
}

void lv_ex_init(void)
{
    /*创建一个预加载对象*/
    lv_obj_t* preload = lv_spinner_create(lv_scr_act(), NULL);//创建加载圆圈
    lv_obj_set_size(preload, 110, 110);//设置尺寸
    lv_spinner_set_arc_length(preload, 80);//设置动态圆弧长度
    lv_obj_align(preload, NULL, LV_ALIGN_CENTER, 0, 15);//相对父对象居中

    static lv_style_t style;
    lv_style_init(&style);

    /*Set a background color and a radius*/
    lv_style_set_radius(&style, LV_STATE_DEFAULT, 10);
    lv_style_set_bg_opa(&style, LV_STATE_DEFAULT, LV_OPA_COVER);
    lv_style_set_bg_color(&style, LV_STATE_DEFAULT, LV_COLOR_WHITE);
    lv_style_set_border_color(&style, LV_STATE_DEFAULT, lv_color_hex(0x01a2b1));
    /*Create an object with the new style*/
    lv_obj_t* obj = lv_obj_create(lv_scr_act(), NULL);
    lv_obj_add_style(obj, LV_OBJ_PART_MAIN, &style);
    lv_obj_set_size(obj, 170, 30);
    lv_obj_align_origo(obj, preload, LV_ALIGN_OUT_TOP_MID, 0, -20);//设置位置

    static lv_style_t style_lab;
    lv_style_init(&style_lab);

    lv_style_set_text_color(&style_lab, LV_STATE_DEFAULT, LV_COLOR_BLACK); //字体颜色
    lv_obj_t* label = lv_label_create(obj, NULL);//创建文本
    lv_obj_add_style(label, LV_LABEL_PART_MAIN, &style_lab);
    lv_label_set_static_text(label,Waitwifi);
    lv_obj_align(label, NULL, LV_ALIGN_CENTER, 0, 0);
    
}

void lv_main_time(void)
{

    static lv_clock_t lv_clock = { 0 };
    ///////////////////////////////////////时钟部分设计
    static lv_style_t style_clock;
    lv_style_reset(&style_clock);
    lv_style_init(&style_clock);
    lv_style_set_bg_color(&style_clock, LV_STATE_DEFAULT, lv_color_hex(0x41485a));
    lv_style_set_border_color(&style_clock, LV_STATE_DEFAULT, lv_color_hex(0x41485a));

    static lv_style_t style_lab_time;
    lv_style_reset(&style_lab_time);
    lv_style_init(&style_lab_time);
    lv_style_set_text_color(&style_lab_time, LV_STATE_DEFAULT, LV_COLOR_ORANGE); //字体颜色
    lv_style_set_text_font(&style_lab_time, LV_STATE_DEFAULT, &lv_font_montserrat_48);//字体大小

    static lv_style_t style_lab_data_weekday;
    lv_style_reset(&style_lab_data_weekday);
    lv_style_init(&style_lab_data_weekday);
    lv_style_set_text_color(&style_lab_data_weekday, LV_STATE_DEFAULT, LV_COLOR_WHITE); //字体颜色
    lv_style_set_text_font(&style_lab_data_weekday, LV_STATE_DEFAULT, &lv_font_montserrat_18);//字体大小

    lv_obj_t* obj_time = lv_obj_create(scr_main, NULL);
    lv_obj_set_size(obj_time, 210, 75); // 设置对象大小
    lv_obj_align(obj_time, scr_main, LV_ALIGN_CENTER, 0, -10);
    lv_obj_add_style(obj_time, LV_OBJ_PART_MAIN, &style_clock);

    lv_clock.time_label = lv_label_create(obj_time, NULL);//创建文本
    lv_obj_add_style(lv_clock.time_label, LV_LABEL_PART_MAIN, &style_lab_time);

    lv_clock.data_label = lv_label_create(obj_time, NULL);
    lv_obj_add_style(lv_clock.data_label, LV_LABEL_PART_MAIN, &style_lab_data_weekday);

    lv_clock.weekday_label = lv_label_create(obj_time, NULL);
    lv_obj_add_style(lv_clock.weekday_label, LV_LABEL_PART_MAIN, &style_lab_data_weekday);
   ///////////////////天气部分设计1
    static lv_style_t style_weather;											// 创建一个风格
    lv_style_init(&style_weather);// 初始化风格
    lv_style_set_text_color(&style_weather, LV_STATE_DEFAULT, lv_color_hex(0x00fefe)); //字体颜色
    lv_style_set_text_font(&style_weather, LV_STATE_DEFAULT, &weather_font40);

    static lv_style_t style_weather_text;											// 创建一个风格
    lv_style_init(&style_weather_text);// 初始化风格
    lv_style_set_text_color(&style_weather_text, LV_STATE_DEFAULT, lv_color_hex(0xff00fa)); //字体颜色
    lv_style_set_text_font(&style_weather_text, LV_STATE_DEFAULT, &weather_text_20);

    lv_obj_t* obj_weather = lv_obj_create(scr_main, NULL);
    lv_obj_set_size(obj_weather, 60, 70); // 设置对象大小
    lv_obj_align(obj_weather, scr_main,LV_ALIGN_IN_TOP_LEFT, 0, 0);
    lv_obj_add_style(obj_weather, LV_OBJ_PART_MAIN, &style_clock);

    lv_clock.weather_label = lv_label_create(obj_weather, NULL);
    lv_obj_add_style(lv_clock.weather_label, LV_LABEL_PART_MAIN, &style_weather);		// 应用效果风格

    lv_clock.weather_text_label = lv_label_create(obj_weather, NULL);
    lv_obj_add_style(lv_clock.weather_text_label, LV_LABEL_PART_MAIN, &style_weather_text);		// 应用效果风格
    ////////////////地点部分设计

    static lv_style_t style_address;
    lv_style_reset(&style_address);
    lv_style_init(&style_address);
    lv_style_set_bg_color(&style_address, LV_STATE_DEFAULT, lv_color_hex(0xff6634));
    lv_style_set_border_color(&style_address, LV_STATE_DEFAULT, lv_color_hex(0xff6634));

    lv_obj_t* obj_address = lv_obj_create(scr_main, NULL);
    lv_obj_set_size(obj_address, 60, 20); // 设置对象大小
    lv_obj_align(obj_address, scr_main, LV_ALIGN_IN_TOP_MID, -30, 10);
    lv_obj_add_style(obj_address, LV_OBJ_PART_MAIN, &style_address);
    if (strcmp(loaction, (char*)"hefei")==0)
    {
        static lv_style_t style_address_text;											// 创建一个风格
        lv_style_init(&style_address_text);// 初始化风格
        lv_style_set_text_color(&style_address_text, LV_STATE_DEFAULT, LV_COLOR_WHITE); //字体颜色
        lv_style_set_text_font(&style_address_text, LV_STATE_DEFAULT, &address_font);

        lv_obj_t* label_address = lv_label_create(obj_address, NULL);
        lv_obj_add_style(label_address, LV_LABEL_PART_MAIN, &style_address_text);		// 应用效果风格
        lv_label_set_text(label_address, "合肥");// 设置显示文本
        lv_obj_align(label_address, lv_obj_get_parent(label_address), LV_ALIGN_CENTER, 0, 0);
    }
    else if (strcmp(loaction, (char*)"beijing") == 0)
    {
        static lv_style_t style_address_text;											// 创建一个风格
        lv_style_init(&style_address_text);// 初始化风格
        lv_style_set_text_color(&style_address_text, LV_STATE_DEFAULT, LV_COLOR_WHITE); //字体颜色
        lv_style_set_text_font(&style_address_text, LV_STATE_DEFAULT, &address_font);

        lv_obj_t* label_address = lv_label_create(obj_address, NULL);
        lv_obj_add_style(label_address, LV_LABEL_PART_MAIN, &style_address_text);		// 应用效果风格
        lv_label_set_text(label_address, "北京");// 设置显示文本
        lv_obj_align(label_address, lv_obj_get_parent(label_address), LV_ALIGN_CENTER, 0, 0);
    }
    else if (strcmp(loaction, (char*)"dalian") == 0)
    {
        static lv_style_t style_address_text;											// 创建一个风格
        lv_style_init(&style_address_text);// 初始化风格
        lv_style_set_text_color(&style_address_text, LV_STATE_DEFAULT, LV_COLOR_WHITE); //字体颜色
        lv_style_set_text_font(&style_address_text, LV_STATE_DEFAULT, &address_font);

        lv_obj_t* label_address = lv_label_create(obj_address, NULL);
        lv_obj_add_style(label_address, LV_LABEL_PART_MAIN, &style_address_text);		// 应用效果风格
        lv_label_set_text(label_address, "大连");// 设置显示文本
        lv_obj_align(label_address, lv_obj_get_parent(label_address), LV_ALIGN_CENTER, 0, 0);
    }
    else if (strcmp(loaction, (char*)"shanghai") == 0)
    {
        static lv_style_t style_address_text;											// 创建一个风格
        lv_style_init(&style_address_text);// 初始化风格
        lv_style_set_text_color(&style_address_text, LV_STATE_DEFAULT, LV_COLOR_WHITE); //字体颜色
        lv_style_set_text_font(&style_address_text, LV_STATE_DEFAULT, &address_font);

        lv_obj_t* label_address = lv_label_create(obj_address, NULL);
        lv_obj_add_style(label_address, LV_LABEL_PART_MAIN, &style_address_text);		// 应用效果风格
        lv_label_set_text(label_address, "上海");// 设置显示文本
        lv_obj_align(label_address, lv_obj_get_parent(label_address), LV_ALIGN_CENTER, 0, 0);
    }
    else if (strcmp(loaction, (char*)"shenzhen") == 0)
    {
        static lv_style_t style_address_text;											// 创建一个风格
        lv_style_init(&style_address_text);// 初始化风格
        lv_style_set_text_color(&style_address_text, LV_STATE_DEFAULT, LV_COLOR_WHITE); //字体颜色
        lv_style_set_text_font(&style_address_text, LV_STATE_DEFAULT, &address_font);

        lv_obj_t* label_address = lv_label_create(obj_address, NULL);
        lv_obj_add_style(label_address, LV_LABEL_PART_MAIN, &style_address_text);		// 应用效果风格
        lv_label_set_text(label_address, "深圳");// 设置显示文本
        lv_obj_align(label_address, lv_obj_get_parent(label_address), LV_ALIGN_CENTER, 0, 0);
    }
    else
    {
        static lv_style_t style_address_text;											// 创建一个风格
        lv_style_init(&style_address_text);// 初始化风格
        lv_style_set_text_color(&style_address_text, LV_STATE_DEFAULT, LV_COLOR_WHITE); //字体颜色
        lv_style_set_text_font(&style_address_text, LV_STATE_DEFAULT, &lv_font_montserrat_16);

        lv_obj_t* label_address = lv_label_create(obj_address, NULL);
        lv_obj_add_style(label_address, LV_LABEL_PART_MAIN, &style_address_text);		// 应用效果风格
        lv_label_set_text_fmt(label_address, "%s", loaction);// 设置显示文本
        lv_obj_align(label_address, lv_obj_get_parent(label_address), LV_ALIGN_CENTER, 0, 0);
    }
    //------------------温度temperature界面

    lv_obj_t* obj_temperature = lv_obj_create(scr_main, NULL);
    lv_obj_set_size(obj_temperature, 60, 40); // 设置对象大小
    lv_obj_align(obj_temperature, obj_address, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
    lv_obj_add_style(obj_temperature, LV_OBJ_PART_MAIN, &style_clock);

    static lv_style_t style_temperature;											// 创建一个风格
    lv_style_init(&style_temperature);// 初始化风格
    lv_style_set_text_color(&style_temperature, LV_STATE_DEFAULT, lv_color_hex(0x22a8f1)); //字体颜色#22a8f1
    lv_style_set_text_font(&style_temperature, LV_STATE_DEFAULT, &lv_font_montserrat_30);

    lv_clock.temperature_label = lv_label_create(obj_temperature, NULL);
    lv_obj_add_style(lv_clock.temperature_label, LV_LABEL_PART_MAIN, &style_temperature);		// 应用效果风格
    //------------------ 其他信息图案
    lv_obj_t* obj_other = lv_obj_create(scr_main, NULL);
    lv_obj_set_size(obj_other, 120, 60); // 设置对象大小
    lv_obj_align(obj_other, scr_main,LV_ALIGN_IN_TOP_RIGHT, 0, 10);
    lv_obj_add_style(obj_other, LV_OBJ_PART_MAIN, &style_clock);

    static lv_style_t style1_other;
    lv_style_reset(&style1_other);
    lv_style_init(&style1_other);
    lv_style_set_text_color(&style1_other, LV_STATE_DEFAULT, lv_color_hex(0xe5cab9)); //字体颜色
    lv_style_set_text_font(&style1_other, LV_STATE_DEFAULT, &other_font);//字体大小

    static lv_style_t style2_other;
    lv_style_reset(&style2_other);
    lv_style_init(&style2_other);
    lv_style_set_text_color(&style2_other, LV_STATE_DEFAULT, lv_color_hex(0xe5cab9)); //字体颜色
    lv_style_set_text_font(&style2_other, LV_STATE_DEFAULT, &lv_font_montserrat_24);//字体大小

    lv_obj_t* other_label_1 = lv_label_create(obj_other, NULL);
    lv_obj_add_style(other_label_1, LV_LABEL_PART_MAIN, &style1_other);		// 应用效果风格
    lv_label_set_text_fmt(other_label_1, "%s风", wind_direction);
    lv_obj_align(other_label_1, lv_obj_get_parent(other_label_1), LV_ALIGN_IN_TOP_LEFT, 0, 0);

    lv_obj_t* other_label_2 = lv_label_create(obj_other, NULL);
    lv_obj_add_style(other_label_2, LV_LABEL_PART_MAIN, &style2_other);		// 应用效果风格
    lv_label_set_text_fmt(other_label_2, "%d", wind_scale);
    lv_obj_align(other_label_2, other_label_1, LV_ALIGN_OUT_RIGHT_MID, 0, 0);

    lv_obj_t* other_label_3 = lv_label_create(obj_other, NULL);
    lv_obj_add_style(other_label_3, LV_LABEL_PART_MAIN, &style1_other);		// 应用效果风格
    lv_label_set_text(other_label_3, "级");// 设置显示文本
    lv_obj_align(other_label_3, other_label_2, LV_ALIGN_OUT_RIGHT_MID, 0, 0);

    lv_obj_t* other_label_4 = lv_label_create(obj_other, NULL);
    lv_obj_add_style(other_label_4, LV_LABEL_PART_MAIN, &style1_other);		// 应用效果风格
    lv_label_set_text(other_label_4, "湿度");// 设置显示文本
    lv_obj_align(other_label_4, lv_obj_get_parent(other_label_4), LV_ALIGN_IN_BOTTOM_LEFT, 0, -5);

    lv_obj_t* other_label_5 = lv_label_create(obj_other, NULL);
    lv_obj_add_style(other_label_5, LV_LABEL_PART_MAIN, &style2_other);		// 应用效果风格
    lv_label_set_text_fmt(other_label_5, ":%d%%", humi);
    lv_obj_align(other_label_5, other_label_4, LV_ALIGN_OUT_RIGHT_MID, 0, 0);

    //------------------左下脚WiFi图案
    lv_obj_t* obj_wifi = lv_obj_create(scr_main, NULL);
    lv_obj_set_size(obj_wifi, 150, 30); // 设置对象大小
    lv_obj_align(obj_wifi, scr_main, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
    lv_obj_add_style(obj_wifi, LV_OBJ_PART_MAIN, &style_clock);

    static lv_style_t style_wifi;
    lv_style_reset(&style_wifi);
    lv_style_init(&style_wifi);
    lv_style_set_text_color(&style_wifi, LV_STATE_DEFAULT, LV_COLOR_WHITE); //字体颜色
    lv_style_set_text_font(&style_wifi, LV_STATE_DEFAULT, &lv_font_montserrat_18);//字体大小

    lv_obj_t* wifi_label = lv_label_create(obj_wifi, NULL);
    lv_obj_add_style(wifi_label, LV_LABEL_PART_MAIN, &style_wifi);		// 应用效果风格
    lv_label_set_text_fmt(wifi_label, LV_SYMBOL_WIFI" %s", wifi_ssid);
    lv_obj_align(wifi_label, lv_obj_get_parent(wifi_label), LV_ALIGN_IN_LEFT_MID, 5, 0);

    lv_task_t* task_timer = lv_task_create(clock_date_task_callback, 200, LV_TASK_PRIO_MID,&lv_clock);  // 创建定时任务，200ms刷新一次
    lv_task_ready(task_timer);
}
