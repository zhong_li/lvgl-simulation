/**
 * @file main
 *
 */

 /*********************
  *      INCLUDES
  *********************/
#define _DEFAULT_SOURCE /* needed for usleep() */

#include <stdlib.h>
#include <unistd.h>

#define SDL_MAIN_HANDLED /*To fix SDL's "undefined reference to WinMain" issue*/

#include "SDL2/SDL.h"
#include "lvgl/lvgl.h"
#include "examples/lv_examples.h"
#include "demos/lv_demos.h"
#include "lv_drivers/display/monitor.h"
#include "lv_drivers/indev/mouse.h"
#include "lv_drivers/indev/keyboard.h"
#include "lv_drivers/indev/mousewheel.h"
//#include "ui/ui.h"
#include "ui/lv_100ask_2048/lv_100ask_2048.h"
  //#include <stdio.h>

static void hal_init(void);

static int tick_thread(void* data);

static int flag_wifi = 0;
static int flag_history = 0;
static int flag_connect_succ = 0;

// bar任务
static void bar_anim(lv_timer_t* t)
{
    static uint32_t x = 0;				// 静态变量Bar可变值
    static uint32_t state = 0;
    static char buf[64];
    lv_obj_t* bar = t->user_data;		// 从任务参数中获取bar对象
    lv_bar_set_value(bar, x, LV_ANIM_OFF);				// 设置值，关闭动画
    switch (state)
    {
    case 0:
        if (!flag_history)
        {
            if (x < 20)
            {
                x++;
            }
            lv_snprintf(buf, sizeof(buf), "Find historical data!");
            //printf("查找历史数据\n");
        }
        else           //到来事件状态改变，进度条设置为20
        {
            state = 1;
            x = 20;
        }
        break;
    case 1:
        if (!flag_wifi)
        {
            if (x < 50)
            {
                x++;
            }
            lv_snprintf(buf, sizeof(buf), "WiFi Connect!");
            //printf("wifi 连接\n");
        }
        else            //到来事件状态改变，进度条设置为50
        {
            state = 2;
            x = 50;
        }
        break;
    case 2:
        if (!flag_connect_succ)
        {
            if (x < 100)
            {
                x++;
            }
            lv_snprintf(buf, sizeof(buf), "Wait for the connection to succeed!");
            //printf("等待连接成功\n");
        }
        else            //到来事件状态改变，进度条设置为100
        {
            state = 3;
            x = 100;
        }
        break;

    default:
        break;
    }
}
static void Events_Simulation(lv_timer_t* t)
{
    static uint32_t time = 0;
    time++;
    if (time == 4)
    {
        flag_history = 1;
    }
    if (time == 7)
    {
        flag_wifi = 1;
    }
    if (time == 9)
    {
        flag_connect_succ = 1;
    }
    if (time >= 10)
    {
        time = 10;
    }
}

void bar_demo(void)
{
    lv_obj_t* bar = lv_bar_create(lv_scr_act());
    lv_obj_set_size(bar, 200, 20);                    // 控件尺寸
    lv_obj_center(bar);                             // 对齐到中心
    lv_bar_set_range(bar, 0, 100);                    // 设置范围，最小值，最大值
    lv_bar_set_start_value(bar, 0, LV_ANIM_OFF);      // 设置开始值，非动画方式
    lv_timer_create(bar_anim, 100, bar);
    lv_timer_create(Events_Simulation, 700, NULL);
}

void transition_demo(void)
{
    static lv_style_transition_dsc_t trans;
    static const lv_style_prop_t trans_props[] = {
        LV_STYLE_WIDTH, LV_STYLE_HEIGHT, LV_STYLE_BG_COLOR, 0,
    };
    lv_style_transition_dsc_init(&trans, trans_props,
        lv_anim_path_ease_in_out, 500, 0, NULL);

    lv_obj_t* obj = lv_obj_create(lv_scr_act());
    lv_obj_center(obj);                             // 对齐到中心

    static lv_style_t style_trans;
    lv_style_init(&style_trans);
    lv_style_set_transition(&style_trans, &trans);

    lv_style_set_bg_color(&style_trans, lv_palette_main(LV_PALETTE_RED));
    lv_style_set_width(&style_trans, 150);
    lv_style_set_height(&style_trans, 60);
    lv_obj_add_style(obj, &style_trans, LV_STATE_PRESSED);

}

static void anim_progress_load(void* obj, int32_t v) {
    lv_bar_set_start_value(obj, v, LV_ANIM_ON);
    lv_bar_set_value(obj, 20 + v, LV_ANIM_ON);
}

void bar_transition_demo(void)
{
    lv_obj_t* bar = lv_bar_create(lv_scr_act());
    lv_bar_set_mode(bar, LV_BAR_MODE_RANGE);

    static lv_style_t style_bg;
    static lv_style_t style_indic;
    lv_style_init(&style_bg);
    lv_style_set_border_color(&style_bg, lv_palette_main(LV_PALETTE_BLUE));
    lv_style_set_border_width(&style_bg, 2);
    lv_style_set_pad_all(&style_bg, 6);
    lv_style_set_radius(&style_bg, 6);
    lv_style_set_anim_time(&style_bg, 1000);
    lv_style_init(&style_indic);
    lv_style_set_bg_opa(&style_indic, LV_OPA_COVER);
    lv_style_set_bg_color(&style_indic, lv_palette_main(LV_PALETTE_BLUE));
    lv_style_set_radius(&style_indic, 3);
    lv_obj_remove_style_all(bar);
    lv_obj_add_style(bar, &style_bg, 0);
    lv_obj_add_style(bar, &style_indic, LV_PART_INDICATOR);
    lv_obj_set_size(bar, 200, 20);
    lv_obj_center(bar);                             // 对齐到中心

    lv_anim_t anim;
    lv_anim_init(&anim);
    lv_anim_set_var(&anim, bar);

    lv_anim_set_exec_cb(&anim, anim_progress_load);
    lv_anim_set_values(&anim, 0, 80);
    lv_anim_set_path_cb(&anim, lv_anim_path_linear);
    lv_anim_set_time(&anim, 1500);
    lv_anim_set_delay(&anim, 0);

    //lv_anim_set_repeat_delay(&anim, 1500);
    lv_anim_set_early_apply(&anim, true);
    lv_anim_set_repeat_count(&anim, LV_ANIM_REPEAT_INFINITE);

    lv_anim_start(&anim);
}

//2048 小游戏 demo
static void game_2048_event_cb(lv_event_t * e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t * obj_2048 = lv_event_get_target(e);
    lv_obj_t * label = lv_event_get_user_data(e);
    
    if(code == LV_EVENT_VALUE_CHANGED) {
        if (lv_100ask_2048_get_best_tile(obj_2048) >= 2048)
            lv_label_set_text(label, "#00b329 YOU WIN! #");
        else if(lv_100ask_2048_get_status(obj_2048))
            lv_label_set_text(label, "#00b329 www.100ask.net: # #ff0000 GAME OVER! #");
        else
            lv_label_set_text_fmt(label, "SCORE: #ff00ff %d #", lv_100ask_2048_get_score(obj_2048));
    }
}

static void new_game_btn_event_handler(lv_event_t * e)
{
    lv_obj_t * obj_2048 = lv_event_get_user_data(e);

    lv_100ask_2048_set_new_game(obj_2048);
}


void lv_100ask_2048_simple_test(void)
{
    /*Create 2048 game*/
    lv_obj_t * obj_2048 = lv_100ask_2048_create(lv_scr_act());
#if LV_FONT_MONTSERRAT_40    
    lv_obj_set_style_text_font(obj_2048, &lv_font_montserrat_40, 0);
#endif
    lv_obj_set_size(obj_2048, 200, 200);
    lv_obj_center(obj_2048);

    /*Information*/
    lv_obj_t * label = lv_label_create(lv_scr_act());
    lv_label_set_recolor(label, true); 
    lv_label_set_text_fmt(label, "SCORE: #ff00ff %d #", lv_100ask_2048_get_score(obj_2048));
    lv_obj_align_to(label, obj_2048, LV_ALIGN_OUT_TOP_RIGHT, 0, -10);

    lv_obj_add_event_cb(obj_2048, game_2048_event_cb, LV_EVENT_ALL, label);

    /*New Game*/
    lv_obj_t * btn = lv_btn_create(lv_scr_act());
    lv_obj_align_to(btn, obj_2048, LV_ALIGN_OUT_TOP_LEFT, 0, -25);
    lv_obj_add_event_cb(btn, new_game_btn_event_handler, LV_EVENT_CLICKED, obj_2048);

    label = lv_label_create(btn);
    lv_label_set_text(label, "New Game");
    lv_obj_center(label);
}


int main(int argc, char** argv) {
    (void)argc; /*Unused*/
    (void)argv; /*Unused*/

    /*Initialize LVGL*/
    lv_init();

    /*Initialize the HAL (display, input devices, tick) for LVGL*/
    hal_init();

    //init_ui();

    //lv_demo_widgets();
    //lv_demo_music();
    //bar_demo();
    //transition_demo();
    //bar_transition_demo();
    lv_100ask_2048_simple_test();
    while (1) {
        /* Periodically call the lv_task handler.
         * It could be done in a timer interrupt or an OS task too.*/
         //lv_timer_handler();
        usleep(lv_timer_handler() * 1000);
    }

    return 0;
}

/**********************
 *   STATIC FUNCTIONS
 **********************/

 /**
  * Initialize the Hardware Abstraction Layer (HAL) for the LVGL graphics
  * library
  */
static void hal_init(void) {
    /* Use the 'monitor' driver which creates window on PC's monitor to simulate a display*/
    monitor_init();
    /* Tick init.
     * You have to call 'lv_tick_inc()' in periodically to inform LittelvGL about
     * how much time were elapsed Create an SDL thread to do this*/
    SDL_CreateThread(tick_thread, "tick", NULL);

    /*Create a display buffer*/
    static lv_disp_draw_buf_t disp_buf1;
    static lv_color_t buf1_1[MONITOR_HOR_RES * 100];
    static lv_color_t buf1_2[MONITOR_HOR_RES * 100];
    lv_disp_draw_buf_init(&disp_buf1, buf1_1, buf1_2, MONITOR_HOR_RES * 100);

    /*Create a display*/
    static lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv); /*Basic initialization*/
    disp_drv.draw_buf = &disp_buf1;
    disp_drv.flush_cb = monitor_flush;
    disp_drv.hor_res = MONITOR_HOR_RES;
    disp_drv.ver_res = MONITOR_VER_RES;
    disp_drv.antialiasing = 1;

    lv_disp_t* disp = lv_disp_drv_register(&disp_drv);

    lv_theme_t* th = lv_theme_default_init(disp, lv_palette_main(LV_PALETTE_BLUE), lv_palette_main(LV_PALETTE_RED), LV_THEME_DEFAULT_DARK, LV_FONT_DEFAULT);
    lv_disp_set_theme(disp, th);

    lv_group_t* g = lv_group_create();
    lv_group_set_default(g);

    /* Add the mouse as input device
     * Use the 'mouse' driver which reads the PC's mouse*/
    mouse_init();
    static lv_indev_drv_t indev_drv_1;
    lv_indev_drv_init(&indev_drv_1); /*Basic initialization*/
    indev_drv_1.type = LV_INDEV_TYPE_POINTER;

    /*This function will be called periodically (by the library) to get the mouse position and state*/
    indev_drv_1.read_cb = mouse_read;
    lv_indev_t* mouse_indev = lv_indev_drv_register(&indev_drv_1);

    keyboard_init();
    static lv_indev_drv_t indev_drv_2;
    lv_indev_drv_init(&indev_drv_2); /*Basic initialization*/
    indev_drv_2.type = LV_INDEV_TYPE_KEYPAD;
    indev_drv_2.read_cb = keyboard_read;
    lv_indev_t* kb_indev = lv_indev_drv_register(&indev_drv_2);
    lv_indev_set_group(kb_indev, g);
    mousewheel_init();
    static lv_indev_drv_t indev_drv_3;
    lv_indev_drv_init(&indev_drv_3); /*Basic initialization*/
    indev_drv_3.type = LV_INDEV_TYPE_ENCODER;
    indev_drv_3.read_cb = mousewheel_read;

    lv_indev_t* enc_indev = lv_indev_drv_register(&indev_drv_3);
    lv_indev_set_group(enc_indev, g);

    /*Set a cursor for the mouse*/
    LV_IMG_DECLARE(mouse_cursor_icon); /*Declare the image file.*/
    lv_obj_t* cursor_obj = lv_img_create(lv_scr_act()); /*Create an image object for the cursor */
    lv_img_set_src(cursor_obj, &mouse_cursor_icon);           /*Set the image source*/
    lv_indev_set_cursor(mouse_indev, cursor_obj);             /*Connect the image  object to the driver*/
}

/**
 * A task to measure the elapsed time for LVGL
 * @param data unused
 * @return never return
 */
static int tick_thread(void* data) {
    (void)data;

    while (1) {
        SDL_Delay(5);
        lv_tick_inc(5); /*Tell LittelvGL that 5 milliseconds were elapsed*/
    }

    return 0;
}
