# LVGL_PC模拟器

## Visual Studio

#### 介绍

选择使用Visual Studio2019模拟LVGL

#### 软件架构

LVGL版本为7.11.0

## VScode

#### 介绍

使用cmake插件，和mingw64工具集进行编译，使用时需要重新配置一下cmake路径

#### 软件架构

目前已经适配两个版本 

分别为 7.11.0 和 8.3.0 可根据不同版本适配不同模拟器。